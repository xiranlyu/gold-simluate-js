export default function before (n, func) {
  // TODO:
  //   Creates a function that invokes func while it's called less than `n` times.
  //   Please read the test to get how it works.
  // <-start-
  if (Number.isNaN(n)) {
    n = 0;
  }
  let invoked = 0;
  return () => {
    invoked++;
    if (invoked < n) {
      func();
    }
  };
  // --end-->
}

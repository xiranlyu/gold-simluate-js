export default function maxMap (array, mappingFunc) {
  // TODO:m
  //   This function will find the maximum mapped value of `array`. The mapped value
  //   for each item should be calculated using `mappingFunc`.
  //
  //   Please read the test to get a basic idea.
  // <-start-
  if (array === null || array === undefined || array.length === 0) {
    return undefined;
  }

  const filteredArray = array.filter(x => x !== null && x !== undefined && !Number.isNaN(x));
  if (filteredArray.length === 0) {
    return undefined;
  }
  const mappedArray = filteredArray.map(x => mappingFunc(x));
  let maxValue = mappedArray[0];
  for (let i = 0; i < mappedArray.length; i++) {
    if (mappedArray[i] >= maxValue) {
      maxValue = mappedArray[i];
    }
  }
  return maxValue;
  // --end-->s
}
